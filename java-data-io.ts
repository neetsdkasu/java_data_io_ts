/* Java Data IO
 * author: Leonardone @ NEETSDKASU
 * MIT License
 */

// ArrayBufferLike, ArrayBufferView are defined in lib.es5.d.ts

class UTFDataFormatException {
	public constructor() {}
}

class JavaDataInput {

	private readonly data: Uint8Array;
	private readonly view: DataView;
	private pos: number = 0;

	public constructor(buf: ArrayBufferLike|ArrayBufferView) {
		if (ArrayBuffer.isView(buf)) {
			const buffer = buf.buffer;
			const offset = buf.byteOffset;
			const length = buf.byteLength;
			this.data = new Uint8Array(buffer, offset, length);
			this.view = new DataView(buffer, offset, length);
		} else {
			this.data = new Uint8Array(buf);
			this.view = new DataView(buf);
		}
	}

	public reset(): void {
		this.pos = 0;
	}

	public readBoolean(): boolean {
		return this.view.getUint8(this.pos++) !== 0;
	}

	public readByte(): number {
		return this.view.getInt8(this.pos++);
	}

	public readChar(): number {
		const c = this.view.getUint16(this.pos, false);
		this.pos += 2;
		return c;
	}

	public readDouble(): number {
		const v = this.view.getFloat64(this.pos, false);
		this.pos += 8;
		return v;
	}

	public readFloat(): number {
		const v = this.view.getFloat32(this.pos, false);
		this.pos += 4;
		return v;
	}

	public readFully(buf: ArrayBufferLike|ArrayBufferView): void {
		if (ArrayBuffer.isView(buf)) {
			const b = new Uint8Array(buf.buffer, buf.byteOffset, buf.byteLength);
			b.set(this.data.subarray(this.pos, this.pos+b.length), 0);
			this.pos += b.length;
		} else {
			const b = new Uint8Array(buf);
			b.set(this.data.subarray(this.pos, this.pos+b.length), 0);
			this.pos += b.length;
		}
	}

	public readInt(): number {
		const v = this.view.getInt32(this.pos, false);
		this.pos += 4;
		return v;
	}

	public readLine(): string {
		const data = this.data;
		let pos = this.pos;
		while (pos < data.length) {
			if (data[pos] === 0x0A) {
				const s = String.fromCharCode(...this.data.subarray(this.pos, pos));
				this.pos = pos+1;
				return s;
			}
			if (data[pos] === 0x0D) {
				const s = String.fromCharCode(...this.data.subarray(this.pos, pos));
				if (pos+1<data.length && data[pos+1] === 0x0A) {
					this.pos = pos+2;
				} else {
					this.pos = pos+1;
				}
				return s;
			}
			pos++;
		}
		const ret = String.fromCharCode(...this.data.subarray(this.pos, pos));
		this.pos = pos;
		return ret;
	}

	public readLong(): bigint {
		const v = this.view.getBigInt64(this.pos, false);
		this.pos += 8;
		return v;
	}

	public readShort(): number {
		const v = this.view.getInt16(this.pos, false);
		this.pos += 2;
		return v;
	}

	public readUnsignedByte(): number {
		return this.view.getUint8(this.pos++);
	}

	public readUnsignedShort(): number {
		const v = this.view.getUint16(this.pos, false);
		this.pos += 2;
		return v;
	}

	public readUTF(): string {
		let blen = this.readUnsignedShort();
		const b = new Uint16Array(blen);
		const data = this.data;
		let pos = this.pos;
		let len = 0;
		while (blen > 0) {
			const x = data[pos++];
			if ((x & 0xf0) === 0xf0) {
				// 0b1111????
				throw new UTFDataFormatException();
			} else if ((x & 0xc0) === 0x80) {
				// 0b10??????
				throw new UTFDataFormatException();
			}
			if (x < 128) {
				b[len++] = x;
				blen--;
			} else if (x < (128|64|32)) {
				const y = data[pos++];
				if ((y & 0xc0) !== 0x80) {
					// not 0b10??????
					throw new UTFDataFormatException();
				}
				b[len++] = ((x & 0x1f) << 6)
							| (y & 0x3f);
				blen -= 2;
			} else {
				const y = data[pos++];
				const z = data[pos++];
				if ((y & 0xc0) !== 0x80) {
					// not 0b10??????
					throw new UTFDataFormatException();
				}
				if ((z & 0xc0) !== 0x80) {
					// not 0b10??????
					throw new UTFDataFormatException();
				}
				b[len++] = ((x & 0x0f) << 12)
							| ((y & 0x3f) << 6)
							| (z & 0x3f);
				blen -= 3;
			}
		}
		const s = String.fromCharCode(...b.subarray(0, len));
		this.pos = pos;
		return s;
	}

	public skipBytes(n: number): number {
		n = Math.min(n, this.data.length - this.pos);
		this.pos += n;
		return n;
	}
}

class JavaDataOutput {

	private readonly data: Uint8Array;
	private readonly view: DataView;
	private pos: number = 0;

	public constructor(buf: ArrayBufferLike|ArrayBufferView) {
		if (ArrayBuffer.isView(buf)) {
			const buffer = buf.buffer;
			const offset = buf.byteOffset;
			const length = buf.byteLength;
			this.data = new Uint8Array(buffer, offset, length);
			this.view = new DataView(buffer, offset, length);
		} else {
			this.data = new Uint8Array(buf);
			this.view = new DataView(buf);
		}
	}

	public reset(): void {
		this.pos = 0;
	}

	public write(buf: ArrayBufferLike|ArrayBufferView): void {
		if (ArrayBuffer.isView(buf)) {
			const b = new Uint8Array(buf.buffer, buf.byteOffset, buf.byteLength);
			this.data.set(b, this.pos);
			this.pos += b.length;
		} else {
			const b = new Uint8Array(buf);
			this.data.set(b, this.pos);
			this.pos += b.length;
		}
	}

	public writeBoolean(b: boolean): void {
		this.view.setUint8(this.pos++, b ? 1 : 0);
	}

	public writeByte(v: number): void {
		this.view.setInt8(this.pos++, v);
	}

	public writeBytes(s: string): void {
		for (let i = 0; i < s.length; i++) {
			this.writeByte(s.charCodeAt(i));
		}
	}

	public writeChar(c: number): void {
		this.view.setUint16(this.pos, c, false);
		this.pos += 2;
	}

	public writeChars(s: string): void {
		for (let i = 0; i < s.length; i++) {
			this.writeChar(s.charCodeAt(i));
		}
	}

	public writeDouble(v: number): void {
		this.view.setFloat64(this.pos, v, false);
		this.pos += 8;
	}

	public writeFloat(v: number): void {
		this.view.setFloat32(this.pos, v, false);
		this.pos += 4;
	}

	public writeInt(v: number): void {
		this.view.setInt32(this.pos, v, false);
		this.pos += 4;
	}

	public writeLong(v: bigint): void {
		this.view.setBigInt64(this.pos, v, false);
		this.pos += 8;
	}

	public writeShort(v: number): void {
		this.view.setInt16(this.pos, v, false);
		this.pos += 2;
	}

	public writeUTF(s: string): void {
		const data = this.data;
		let pos = this.pos + 2;
		for (let i = 0; i < s.length; i++) {
			const c = s.charCodeAt(i);
			if (0 < c && c < 0x0080) {
				data[pos++] = c;
			} else if (c < 0x0800) {
				data[pos++] = 128|64|(c >>> 6);
				data[pos++] = 128|(63 & c);
			} else {
				data[pos++] = 128|64|32|(c >>> 12);
				data[pos++] = 128|(63 & (c >>> 6));
				data[pos++] = 128|(63 & c);
			}
		}
		const size = pos - (this.pos + 2);
		if (size > 65535) {
			throw new UTFDataFormatException();
		}
		this.writeShort(size);
		this.pos = pos;
	}

	public static sizeHintWriteUTF(s: string): number {
		let size = 2; // writeShort(ByteLength)
		for (let i = 0; i < s.length; i++) {
			const c = s.charCodeAt(i);
			if (0 < c && c < 0x0080) {
				size++;
			} else if (c < 0x0800) {
				size += 2;
			} else {
				size += 3;
			}
		}
		return size;
	}
}