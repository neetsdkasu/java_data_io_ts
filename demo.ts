/* Java Data IO Demo
 * author: Leonardone @ NEETSDKASU
 * MIT License
 */

function javaDataIODemo(): void {
	const ul = <HTMLElement>document.querySelector('ul');
	ul.innerHTML = '';
	const ts: number[] = [0, 1, 2, 3, 4, 5, 6, 7];
	for (let i = 7; i > 0; i--) {
		const j = Math.floor(Math.random()*(i+1));
		const t = ts[i];
		ts[i] = ts[j];
		ts[j] = t;
	}
	const ss: string[] = ['ABCD', 'xyz', '987', 'あいう', '漢字', '１２３', '○Π〈Å', '𝟘𝟙𝟚𝟛😃'];
	let s = '';
	for (let i = Math.floor(Math.random()*10)+6; i > 0; i--) {
		const x = [...ss[Math.floor(Math.random()*ss.length)]];
		s += x[Math.floor(Math.random()*x.length)];
	}
	let len = JavaDataOutput.sizeHintWriteUTF(s);
	const buf = new ArrayBuffer(len+8+8+4+4+2+1+1);
	const dout = new JavaDataOutput(buf);
	for (const t of ts) {
		let v = Math.random() - 0.5;
		switch (t) {
		case 0:
			dout.writeUTF(s);
			ul.appendChild(document.createElement('li'))
				.textContent = `writeUTF("${s}")`;
			break;
		case 1:
			v *= (-10) ** Math.floor(v*80);
			dout.writeDouble(v);
			ul.appendChild(document.createElement('li'))
				.textContent = `writeDouble(${v})`;
			break;
		case 2:
			const bg = BigInt(Math.floor(v*1e8))
				* BigInt(Math.abs(Math.floor(v*1e8)));
			dout.writeLong(bg);
			ul.appendChild(document.createElement('li'))
				.textContent = `writeLong(${bg})`;
			break;
		case 3:
			v *= (-10) ** Math.floor(v*80);
			dout.writeFloat(v);
			const f = new Float32Array(1);
			f[0] = v;
			ul.appendChild(document.createElement('li'))
				.textContent = `writeFloat(${f[0]})`;
			break;
		case 4:
			v = Math.floor(v*4e9);
			dout.writeInt(v);
			ul.appendChild(document.createElement('li'))
				.textContent = `writeInt(${v})`;
			break;
		case 5:
			v = Math.floor(v*6.4e4);
			dout.writeShort(v);
			ul.appendChild(document.createElement('li'))
				.textContent = `writeShort(${v})`;
			break;
		case 6:
			v = Math.floor(v*250);
			dout.writeByte(v);
			ul.appendChild(document.createElement('li'))
				.textContent = `writeByte(${v})`;
			break;
		case 7:
			dout.writeBoolean(v < 0);
			ul.appendChild(document.createElement('li'))
				.textContent = `writeBoolean(${v < 0})`;
			break;
		}
	}
	const din = new JavaDataInput(buf);
	let dst = '';
	let code = '';
	for (const t of ts) {
		code += '\t\tSystem.out.println(';
		switch (t) {
		case 0:
			code += '"UTF: " + dis.readUTF());\n';
			dst += `readUTF: "${din.readUTF()}"
`;
			break;
		case 1:
			code += '"Double: " + dis.readDouble());\n';
			dst += `readDouble: ${din.readDouble()}
`;
			break;
		case 2:
			code += '"Long: " + dis.readLong());\n';
			dst += `readLong: ${din.readLong()}
`;
			break;
		case 3:
			code += '"Float: " + dis.readFloat());\n';
			dst += `readFloat: ${din.readFloat()}
`;
			break;
		case 4:
			code += '"Int: " + dis.readInt());\n';
			dst += `readInt: ${din.readInt()}
`;
			break;
		case 5:
			code += '"Short: " + dis.readShort());\n';
			dst += `readShort: ${din.readShort()}
`;
			break;
		case 6:
			code += '"Byte: " + dis.readByte());\n';
			dst += `readByte: ${din.readByte()}
`;
			break;
		case 7:
			code += '"Boolean: " + dis.readBoolean());\n';
			dst += `readBoolean: ${din.readBoolean()}
`;
			break;
		}
	}
	const ub = new Uint8Array(buf);
	document.querySelector('output')!
		.textContent = `size: ${ub.length}
data: [${ub}]

${dst}`;
	document.querySelector('code')!
		.textContent = `import java.io.*;

class Program {
	public static void main(String[] args) throws Exception {
		byte[] buf = new byte[]{${new Int8Array(buf)}};
		InputStream bais = new ByteArrayInputStream(buf);
		DataInput dis = new DataInputStream(bais);
		${code.trim()}
	}
}
`;
}

function javaDataIOTest(): void {

	let bufSize = 0;

	const utfData = 'あいうDx';
	bufSize += JavaDataOutput.sizeHintWriteUTF(utfData);

	const doubleData = -1.23456789e40;
	bufSize += 8;

	const floatData = (() => {
		const f = new Float32Array(1);
		f[0] = -9.87654321e-10;
		return f[0];
	})();
	bufSize += 4;

	const longData = 100007n+(2n**35n)+(2n**41n)+(2n**44n);
	bufSize += 8;

	const intData = 100007;
	bufSize += 4;

	const shortData = -20009;
	bufSize += 2;

	const unsignedShortData = 65001;
	bufSize += 2;

	const byteData = -128;
	bufSize += 1;

	const unsignedByteData = 250;
	bufSize += 1;

	const trueData = true;
	bufSize += 1;

	const falseData = false;
	bufSize += 1;

	const bytesData = 'abcde12345';
	bufSize += bytesData.length;

	const linesData = 'SAMPLE\nsample\nABCDE';
	bufSize += linesData.length;

	const buf = new ArrayBuffer(bufSize);

	const jdo = new JavaDataOutput(buf);

	jdo.writeUTF(utfData);
	jdo.writeDouble(doubleData);
	jdo.writeFloat(floatData);
	jdo.writeLong(longData);
	jdo.writeInt(intData);
	jdo.writeShort(shortData);
	jdo.writeShort(unsignedShortData);
	jdo.writeByte(byteData);
	jdo.writeByte(unsignedByteData);
	jdo.writeBoolean(trueData);
	jdo.writeBoolean(falseData);
	jdo.writeBytes(bytesData);
	jdo.writeBytes(linesData);

	console.log(new Int8Array(buf));

	const jdi = new JavaDataInput(buf);

	console.log(utfData, jdi.readUTF());
	console.log(doubleData, jdi.readDouble());
	console.log(floatData, jdi.readFloat());
	console.log(longData, jdi.readLong());
	console.log(intData, jdi.readInt());
	console.log(shortData, jdi.readShort());
	console.log(unsignedShortData, jdi.readUnsignedShort());
	console.log(byteData, jdi.readByte());
	console.log(unsignedByteData, jdi.readUnsignedByte());
	console.log(trueData, jdi.readBoolean());
	console.log(falseData, jdi.readBoolean());
	const bytesBuf = new ArrayBuffer(bytesData.length);
	jdi.readFully(bytesBuf)
	console.log(bytesData, String.fromCharCode(...new Uint8Array(bytesBuf)));
	console.log(linesData);
	console.log(`"${jdi.readLine()}"`);
	console.log(`"${jdi.readLine()}"`);
	console.log(`"${jdi.readLine()}"`);


	const b2 = new Uint8Array([48, -94, 48, -85, 48, -75, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 48, -65, 48, -54, 48, -49, 48, -34, 48, -28, 48, -23, 48, -17]);

	const jdi2 = new JavaDataInput(b2.buffer);
	for (const ch of [...'アカサ']) {
		console.log(ch, String.fromCharCode(jdi2.readChar()));
	}
	console.log(31, jdi2.skipBytes(31));
	for (const ch of [...'タナハマヤラワ']) {
		console.log(ch, String.fromCharCode(jdi2.readChar()));
	}

	const buf2 = new ArrayBuffer(b2.length);
	const jdo2 = new JavaDataOutput(buf2);
	for (const ch of [...'アカサ']) {
		jdo2.writeChar(ch.charCodeAt(0));
	}
	const ubs = new Uint8Array(31);
	for (let i = 0; i < 31; i++) {
		ubs[i] = i;
	}
	jdo2.write(ubs.buffer);
	jdo2.writeChars('タナハマヤラワ');

	console.log(b2);
	console.log(new Uint8Array(buf2));
}

javaDataIOTest();
javaDataIODemo();

document.querySelector('button')!
	.addEventListener('click', () => javaDataIODemo());
